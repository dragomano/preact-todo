import TodoList from './components/TodoList'

function App() {
  return (
    <TodoList title="Список дел" />
  )
}

export default App
