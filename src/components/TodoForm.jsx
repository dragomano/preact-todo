import { useRef } from 'preact/hooks';

function TodoForm(props) {
  const inputRef = useRef(null)

  const addTodo = () => {
    props.onSubmit(inputRef.current.value)
    inputRef.current.value = ''
    inputRef.current.focus()
  };

  const handleKeyDown = (e) => {
    if (e.keyCode !== 13) return

    addTodo()
  }

  return (
    <div class="p-4 bg-gray-100">
      <div class="flex items-center">
        <input
          ref={inputRef}
          type="text"
          class="flex-1 mr-2 py-2 px-4 rounded-md border border-gray-300"
          placeholder="Новая задача"
          autofocus
          onKeyDown={handleKeyDown}
        />
        <button
          class="bg-blue-500 hover:bg-blue-600 text-white py-2 px-4 rounded-md"
          onClick={addTodo}
        >
          Добавить
        </button>
      </div>
    </div>
  )
}

export default TodoForm
