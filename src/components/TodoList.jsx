import { useReducer, useEffect } from 'preact/hooks';
import TodoItem from './TodoItem';
import TodoForm from './TodoForm';

const taskReducer = (todos, action) => {
  switch (action.type) {
    case 'add':
      return [
        ...todos,
        {
          id: Date.now(),
          title: action.title,
          completed: false,
        },
      ];

    case 'toggle':
      return todos.map((t) =>
        t.id === action.id ? { ...t, completed: !t.completed } : t
      );

    case 'delete':
      return todos.filter((t) => t.id !== action.id);

    case 'set':
      return action.todos;

    default:
      throw new Error('Неизвестное действие: ' + action.type);
  }
}

function TodoList(props) {
  const [todos, dispatch] = useReducer(taskReducer, []);

  useEffect(() => {
    fetch('https://dummyapi.online/api/todos')
      .then((response) => response.json())
      .then((data) => {
        dispatch({ type: 'set', todos: data.slice(0, 10) });
      });
  }, []);

  const addTodo = (title) => {
    if (!title) return;
    dispatch({ type: 'add', title });
  };

  const toggleTodo = (id) => {
    dispatch({ type: 'toggle', id });
  };

  const deleteTodo = (id) => {
    dispatch({ type: 'delete', id });
  }

  return (
    <div class='max-w-sm md:max-w-lg mx-auto my-10 bg-white rounded-md shadow-md overflow-hidden'>
      <h1 class='text-2xl font-bold text-center py-4 bg-gray-100'>{props.title}</h1>
      {todos.length > 0 && (
        <ul class='list-none p-4'>
          {todos.map((todo) => (
            <TodoItem key={todo.id} todo={todo} onToggle={toggleTodo} onRemove={deleteTodo} />
          ))}
        </ul>
      )}
      <TodoForm onSubmit={addTodo} />
    </div>
  )
}

export default TodoList
